class CreateGuides < ActiveRecord::Migration
  def change
    create_table :guides do |t|
      t.string :Topic
      t.text :Description

      t.timestamps
    end
  end
end
