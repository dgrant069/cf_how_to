class Step < ActiveRecord::Base
  attr_accessible :step, :guide_id
  belongs_to :guide
end
