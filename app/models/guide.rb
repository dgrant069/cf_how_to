class Guide < ActiveRecord::Base
  attr_accessible :Description, :Topic
  has_many :steps, :dependent => :destroy
end
